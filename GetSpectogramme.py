import matplotlib.pyplot as plt
import os
import soundfile as sf
import pydub

from pydub import AudioSegment
pydub.AudioSegment.converter  = "C:/ffmpeg/bin/ffmpeg.exe"
from pydub.utils import make_chunks
from random import randint


def get_spectogram():
    pathScript = os.path.dirname(os.path.realpath(__file__))
    for folders in os.listdir("music"):
        for files in os.listdir("music/"+folders) :
            path =  'Spectogram/Style/' + folders
            print(files)
            print(folders)
            split_audio(str(pathScript) + '/music/' + folders + "/" + files, path,files)

def graph_spectrogram(fileName,count):
    data, rate = sf.read(fileName)
    fig, ax = plt.subplots(1)
    fig.set_size_inches(0.428, 0.428)
    fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
    ax.axis('on')
    ax.specgram(x=data, Fs=rate, noverlap=384, NFFT=512)
    fig.savefig(fileName[:-4] + str(count) + ".png", dpi=300, frameon='false')

def split_audio(filename, folder, newfilename):
    myaudio = AudioSegment.from_file(filename, "wav")
    chunk_length_ms = 2000  # pydub calculates in millisec
    chunks = make_chunks(myaudio, chunk_length_ms)  # Make chunks of x sec
    # Export all of the individual chunks as wav files
    for i, chunk in enumerate(chunks):
        chunk_name = folder + "/" + newfilename
        print("exporting", chunk_name)
        chunk.export(chunk_name, format="wav")
        graph_spectrogram(chunk_name,i)
        os.remove(chunk_name)

if __name__ == '__main__':
    get_spectogram()