# musicFlow
URL site: https://1635235.wixsite.com/musicflow

Installation requise :
+ python 3.6.x
+ tensorflow
+ keras
+ d'autres venv (numpy,pickle,pyaudio,matplotlib,soundfile) ... a voir

Fonction pour découper les chansons:
+ python main.py slice

Fonction pour trainer l'ai(long):
+ python main.py trainer

Fonction pour tester une chanson(rapide):
+ python main.py test
