# -*- coding: utf-8 -*-
import random
import string
import os
import sys
import numpy as np
import keras
import operator

from keras.preprocessing import image
import GetSong as record
import Setting as setting
import GetSpectogramme as specto
from tensorflow import reset_default_graph
import tensorflow as tf
reset_default_graph()
import argparse
from keras import optimizers,layers,models
from keras.models import Model
from keras.layers import Input, Dense, Conv2D, Dropout, Flatten, MaxPooling2D
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("mode", help="Trains or tests the CNN", nargs='+', choices=["train","test","slice"])
args = parser.parse_args()

#List genres
genres = os.listdir(setting.slicesPath)
nbClasses = len(genres)


print("--------------------------")
print("| ** Config ** ")
print("| Genre List: {}".format(genres))
print("| Slice size: {}".format(setting.sliceSize))
print("--------------------------")

def create_model():
    model = models.Sequential()
    model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(128, 128, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(2, 2))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(2, 2))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D(2, 2))
    model.add(layers.Flatten())
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(512, activation='relu'))
    model.add(layers.Dense(4, activation='softmax'))

    model.compile(optimizer=tf.train.AdamOptimizer(),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    print(model.summary())
    return model

if "slice" in args.mode:
    specto.get_spectogram()

if "train" in args.mode:
    # Create or load new dataset
    if os.path.isfile('MusicBot.h5'):
        model = create_model()
        model.load_weights('MusicBot.h5')
    else :
        model = create_model()

    #Train the model
    print("[+] Training the model...")

    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

    validation_datagen = ImageDataGenerator(rescale=1.255)
    pathScript = os.path.dirname(os.path.realpath(__file__))
    datasetPath = pathScript + "/Spectogram/Style/"

    print(genres)
    train_generator = train_datagen.flow_from_directory(datasetPath,target_size=(setting.sliceSize, setting.sliceSize),color_mode="rgb", batch_size=32, class_mode='categorical')

    print(train_generator)

    validation_generator = validation_datagen.flow_from_directory(datasetPath, target_size=(setting.sliceSize, setting.sliceSize),color_mode="rgb", batch_size=32, class_mode='categorical')

    model.fit_generator(train_generator, epochs=setting.nbEpoch, steps_per_epoch=setting.steps,#Training settings
                            validation_data=validation_generator, validation_steps=setting.validation, workers=1)

    print("Model trained! ✅")

    #Save trained model
    print("[+] Saving the weights...")
    model.save_weights('MusicBot.h5')
    print("[+] Weights saved! ✅💾")

if "test" in args.mode:
    record.main()
    model = create_model()
    model.load_weights('MusicBot.h5')

    pathScript = os.path.dirname(os.path.realpath(__file__))
    testPath = pathScript + "/TestSong/Images"

    labels_index = {"Classique": 0, "Hip-Hop": 1, "Pop": 2, "Rock": 3}
    array_percentage_accuracy = [0,0,0,0]
    winningLabel = 0
    t = 0
    for img in os.listdir(testPath):
        i = 0
        img_path = testPath + "/" + img
        print("Prediction for \"%s\": " % (img))
        img = image.load_img(img_path, target_size=(128, 128))
        img_tensor = image.img_to_array(img)  # (height, width, channels)
        img_tensor = np.expand_dims(img_tensor,
                                    axis=0)  # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
        img_tensor /= 255.  # imshow expects values in the range [0, 1]
        predictions = model.predict(img_tensor)
        for label in labels_index:
            print("\t%s ==> %f" % (label, predictions[0][i]))
            winningLabel = predictions.argmax(axis=-1)[0]
            i = i + 1
        valueToIncrement = array_percentage_accuracy[winningLabel] + 1
        array_percentage_accuracy[winningLabel] = valueToIncrement
        t = t +1
        print("The Ai thinks its : ",genres[winningLabel])

    index, value = max(enumerate(array_percentage_accuracy), key=operator.itemgetter(1))
    accuracy = 100 * float(value) / float(t)
    value = genres[index]
    print("The AI is",accuracy,"% sure it is :",value)




